package arc.net;

import arc.*;
import arc.util.*;

import java.io.*;

public class ProxyServer extends Server{
    private final Server server;

    /**
     * @param writeBufferSize One buffer of this size is allocated for each connected
     * client. Objects are serialized to the write buffer where the
     * bytes are queued until they can be written to the TCP socket.
     * <p>
     * Normally the socket is writable and the bytes are written
     * immediately. If the socket cannot be written to and enough
     * serialized objects are queued to overflow the buffer, then the
     * connection will be closed.
     * <p>
     * The write buffer should be sized at least as large as the
     * largest object that will be sent, plus some head room to allow
     * for some serialized objects to be queued in case the buffer is
     * temporarily not writable. The amount of head room needed is
     * dependent upon the size of objects being sent and how often
     * they are sent.
     * @param objectBufferSize One (using only TCP) or three (using both TCP and UDP) buffers
     * of this size are allocated. These buffers are used to hold the
     * bytes for a single object graph until it can be sent over the
     * network or deserialized.
     * <p>
     * The object buffers should be sized at least as large as the
     */
    public ProxyServer(Server server){
        super(server.writeBufferSize, server.objectBufferSize, server.serializer);
        this.server = server;
    }
}
